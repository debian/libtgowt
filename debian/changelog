libtgowt (0~git20230615.a45d8b8+dfsg-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix building with new ffmpeg (Closes: #1078287).

 -- Andrey Rakhmatullin <wrar@debian.org>  Wed, 11 Sep 2024 15:50:50 +0500

libtgowt (0~git20230615.a45d8b8+dfsg-2) unstable; urgency=medium

  * New Unbundle-libyuv.patch and libyuv-dev build dependency.
    - This should fix final linking on RISC-V 64bit.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sat, 24 Jun 2023 17:21:00 +0300

libtgowt (0~git20230615.a45d8b8+dfsg-1) unstable; urgency=medium

  * Update to the latest upstream commit.
  * Exclude libSRTP from the orig tarball.
  * Bring back Ignore-sanitize-attr.patch to fix RISC-V 64bit build.
  * Remove outdated Packaged-PipeWire.patch which is no longer needed.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Tue, 20 Jun 2023 23:15:09 +0300

libtgowt (0~git20230105.5098730+dfsg-2) unstable; urgency=medium

  * New Unbundle-libSRTP.patch.
    - Link against the updated library from the libsrtp2-dev package which
      already has a fix of OpenSSL 3.0.0 incompatibility.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sun, 07 May 2023 13:37:20 +0300

libtgowt (0~git20230105.5098730+dfsg-1) unstable; urgency=medium

  * Update to the latest upstream commit.
  * Increase minimal abseil version to 20220623.0.
  * Remove patches:
    - Conflicting Avoid-SSE2-on-i386.patch until bug#995194 is fixed.
    - Prefer-std-bit-over-absl-bits.patch since backporting to bullseye
      requires more changes.
    - Ignore-sanitize-attr.patch because upstream already seems compatible
      with GCC.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Tue, 10 Jan 2023 23:20:23 +0300

libtgowt (0~git20220508.10d5f4b+dfsg-1) unstable; urgency=medium

  * Update to the latest upstream commit.
  * Remove PipeWire from Files-Excluded in debian/copyright.
  * Remove Fix-loading-libdrm-before-first-use.diff taken from upstream repository.
  * Refresh remaining patches resolving conflicts.
  * New build dependencies, libgl-dev and libegl-dev, remove no longer needed
    libepoxy-dev.
  * Update copyright info, new CRC32C library and removed usrsctp.
  * Remove a workaround for PACKAGE_* macros in <config.h> headers.
  * Bump standards version to 4.6.1, no related changes.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sat, 02 Jul 2022 14:46:00 +0300

libtgowt (0~git20220202.d618d0b+dfsg-2) unstable; urgency=medium

  * Apply 347400dc upstream commit to fix screen sharing crash in Wayland.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Wed, 09 Feb 2022 18:59:44 +0300

libtgowt (0~git20220202.d618d0b+dfsg-1) unstable; urgency=medium

  * Update to the latest upstream commit.
  * Adjust build scripts.
    - New dependencies: libdrm-dev, libepoxy-dev, libgbm-dev, libvpx-dev.
    - Remove support of PipeWire 0.2, no longer needed for buster-backports.
  * Patches:
    - Rewrite Packaged-PipeWire.patch.
    - New Backport-to-stable-libvpx.patch in main branch for easy backporting.
    - New Prefer-std-bit-over-absl-bits.patch to fix build against non-updated
      Abseil in unstable.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sat, 05 Feb 2022 22:49:15 +0300

libtgowt (0~git20211207.d5c3d43+dfsg-2) unstable; urgency=medium

  * New Better-denormal-check.patch to fix FTBFS on armel.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sun, 12 Dec 2021 16:24:53 +0300

libtgowt (0~git20211207.d5c3d43+dfsg-1) unstable; urgency=medium

  [ Nicholas Guriev ]
  * Update to the latest upstream commit.
  * Build against system abseil. Mark this library as transitive dependency.
  * Revert "Define NO_MAIN_THREAD_WRAPPING macro through d/rules".
    - Now upstream CMake scripts define this macro.

  [ Gianfranco Costamagna ]
  * Build depend on unversioned libjpeg-dev to fix an installation failure.
    (Closes: #992865)

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sun, 12 Dec 2021 00:29:57 +0300

libtgowt (0~git20210627.91d836d+dfsg-3) unstable; urgency=medium

  * Upload to unstable.
  * Extend Packaged-PipeWire.patch for 0.2 version for easy backporting.
  * Bump Standards-Version to 4.6.0, no related changes.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Thu, 19 Aug 2021 21:14:30 +0300

libtgowt (0~git20210627.91d836d+dfsg-2) experimental; urgency=medium

  * Automatically collect transitive dependencies.
  * New Ignore-sanitize-attr.patch.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sun, 01 Aug 2021 11:41:28 +0300

libtgowt (0~git20210627.91d836d+dfsg-1) experimental; urgency=medium

  * Update to the latest upstream commit.
  * Refine minimal CMake version, 3.16.0.
  * New build dependencies, PipeWire, Python, GLib, and X11.
  * Update get-orig-source target.
    - Repack to exclude RNNoise non-free model.
  * Update package metadata to fit the upstream code.
  * Update copyright info.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Sat, 31 Jul 2021 16:49:05 +0300

libtgowt (0~git20210124.be23804+ds-3) unstable; urgency=medium

  * Mention missing libprotobuf-dev transitive dependency.
  * Move sources requiring SSE2 to separate sublibrary.
  * Define NO_MAIN_THREAD_WRAPPING macro through d/rules. (Closes: #982556)

 -- Nicholas Guriev <guriev-ns@ya.ru>  Fri, 12 Feb 2021 15:53:58 +0300

libtgowt (0~git20210124.be23804+ds-2) unstable; urgency=medium

  * Require yasm only for x86 based platforms.
  * Fix build on armhf due to incorrect usage of DEB_HOST_ARCH_CPU.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Thu, 04 Feb 2021 21:06:34 +0300

libtgowt (0~git20210124.be23804+ds-1) unstable; urgency=medium

  * Update to the latest upstream commit.
  * New debian/watch file for tracking upstream commits.
  * Do rebuilding protobuf headers.

 -- Nicholas Guriev <guriev-ns@ya.ru>  Wed, 03 Feb 2021 14:02:37 +0300

libtgowt (0~git20201218.6eaebec+ds-2) unstable; urgency=medium

  * Fix porting issues on release architectures.
    - Disable NEON extension on armhf
    - Add Avoid-SSE2-on-i386.patch
    - Add Convert-endianness.patch

 -- Nicholas Guriev <guriev-ns@ya.ru>  Thu, 28 Jan 2021 22:07:04 +0300

libtgowt (0~git20201218.6eaebec+ds-1) unstable; urgency=low

  * Initial upload. (Closes: #977606)

 -- Nicholas Guriev <guriev-ns@ya.ru>  Wed, 23 Dec 2020 17:26:25 +0300
